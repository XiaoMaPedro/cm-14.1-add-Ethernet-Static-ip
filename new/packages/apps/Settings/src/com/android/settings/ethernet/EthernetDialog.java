/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.settings.ethernet;

import com.android.settings.R;
import com.android.settings.ethernet.ip.IPView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RadioButton;
import android.util.Slog;
import android.view.inputmethod.InputMethodManager;
import android.net.IpConfiguration;
import android.net.IpConfiguration.IpAssignment;
import android.net.IpConfiguration.ProxySettings;
import android.os.Environment;
import android.util.SparseArray;
import android.net.StaticIpConfiguration;
import android.net.EthernetManager;
import android.text.TextUtils;
import android.net.LinkAddress;
import android.net.NetworkUtils;
import android.net.ConnectivityManager;
import android.net.LinkProperties;
import com.android.settings.Utils;
import android.widget.Toast;
import android.net.EthernetManager;
import android.provider.Settings.Global;

import java.net.InetAddress;
import java.net.Inet4Address;
import java.net.UnknownHostException;
import java.util.Iterator;

class EthernetDialog extends AlertDialog implements DialogInterface.OnClickListener, DialogInterface.OnShowListener,
        DialogInterface.OnDismissListener{
    private final String TAG = "EthConfDialog";
    private static final boolean localLOGV = false;

    /* This value comes from "wifi_ip_settings" resource array */
    private static final int DHCP = 0;
    private static final int STATIC_IP = 1;
    private IpAssignment mIpAssignment = IpAssignment.DHCP;
	private StaticIpConfiguration mStaticIpConfiguration;
    private IpConfiguration mIpConfiguration;

    private View mView;
    private RadioButton mConTypeDhcp;
    private RadioButton mConTypeManual;
    private IPView mIpaddr;
    private IPView mDns1;
    private IPView mDns2;
    private IPView mGw;
    private IPView mMask;
	//private EditText mprefix;

    private Context mContext;
	private EthernetManager mEthManager;
	private ConnectivityManager mCM;
	
    public EthernetDialog(Context context,EthernetManager EthManager,ConnectivityManager cm) {
        super(context);
        mContext = context;
		mEthManager = EthManager;
		mCM = cm;
        buildDialogContent(context);
        setOnShowListener(this);
        setOnDismissListener(this);
    }

    public void onShow(DialogInterface dialog) {
        if (localLOGV) Slog.d(TAG, "onShow");
		UpdateInfo();
        // soft keyboard pops up on the disabled EditText. Hide it.
        InputMethodManager imm = (InputMethodManager)mContext.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(),
                InputMethodManager.HIDE_IMPLICIT_ONLY);
    }

    public void onDismiss(DialogInterface dialog) {
        if (localLOGV) Slog.d(TAG, "onDismiss");
    }

	public void UpdateInfo() {
		int enable = Global.getInt(mContext.getContentResolver(), Global.ETHERNET_ON, 0);
		if(enable == EthernetManager.ETHERNET_STATE_ENABLED) {
		//if(mEthManager.isAvailable()) {
			IpConfiguration ipinfo = mEthManager.getConfiguration();
			if(ipinfo != null) {
				if(ipinfo.ipAssignment == IpAssignment.DHCP) {
					mConTypeDhcp.setChecked(true);
                    mIpaddr.setEnabled(false);
		            mMask.setEnabled(false);
		            mGw.setEnabled(false);
                    mDns1.setEnabled(false);
                    mDns2.setEnabled(false);
					//mprefix.setEnabled(false);
                    mIpaddr.clear();
                    mMask.clear();
					mGw.clear();
					mDns1.clear();
                    mDns2.clear();
					//mprefix.setText("");
					/*if(mCM != null) {
						LinkProperties lp  = mCM.getLinkProperties(ConnectivityManager.TYPE_ETHERNET);
						if(lp != null) {
							mIpaddr.setIp(formatIpAddresses(lp));
						}
					}*/
				} else {
					mConTypeManual.setChecked(true);
                    mIpaddr.setEnabled(true);
                    mDns1.setEnabled(true);
                    mDns2.setEnabled(true);
		            mGw.setEnabled(true);
		            mMask.setEnabled(true);
					//mprefix.setEnabled(true);
					StaticIpConfiguration staticConfig = ipinfo.getStaticIpConfiguration();
					if (staticConfig != null) {
						if (staticConfig.ipAddress != null) {
                            mIpaddr.setIp(staticConfig.ipAddress.getAddress().getHostAddress());
		                    //mprefix.setText(Integer.toString(staticConfig.ipAddress.getNetworkPrefixLength()));
						}
                        if (!TextUtils.isEmpty(staticConfig.domains)) {
                            mMask.setIp(staticConfig.domains);
                        }
		                if (staticConfig.gateway != null) {
		                    mGw.setIp(staticConfig.gateway.getHostAddress());
		                }
                        int i = 0;
		                Iterator<InetAddress> dnsIterator = staticConfig.dnsServers.iterator();
                        while(dnsIterator.hasNext()) {
                            InetAddress ia = dnsIterator.next();
                            if (i++ == 0) {
                                mDns1.setIp(ia.getHostAddress());
                            } else {
                                mDns2.setIp(ia.getHostAddress());
                            }
                        }
					}
				}
			}
		}
	}

    public int buildDialogContent(Context context) {
        this.setTitle(R.string.eth_config_title);
        this.setView(mView = getLayoutInflater().inflate(R.layout.ethernet_configure, null));
        mConTypeDhcp = (RadioButton) mView.findViewById(R.id.dhcp_radio);
        mConTypeManual = (RadioButton) mView.findViewById(R.id.manual_radio);
        mIpaddr = (IPView)mView.findViewById(R.id.ipaddr_edit);
		//mprefix = (EditText)mView.findViewById(R.id.prefix_edit);
        mMask = (IPView)mView.findViewById(R.id.netmask_edit);
        mGw = (IPView)mView.findViewById(R.id.eth_gw_edit);
        mDns1 = (IPView)mView.findViewById(R.id.eth_dns_edit1);
        mDns2 = (IPView)mView.findViewById(R.id.eth_dns_edit2);

        mConTypeDhcp.setChecked(true);
        mConTypeManual.setChecked(false);
        mIpaddr.setEnabled(false);
        mMask.setEnabled(false);
		//mprefix.setEnabled(false);
        mGw.setEnabled(false);
        mDns1.setEnabled(false);
        mDns2.setEnabled(false);

        mConTypeManual.setOnClickListener(new RadioButton.OnClickListener() {
            public void onClick(View v) {
                mIpaddr.setEnabled(true);
                mDns1.setEnabled(true);
                mDns2.setEnabled(true);
                mGw.setEnabled(true);
                mMask.setEnabled(true);
				//mprefix.setEnabled(true);
				mIpAssignment = IpAssignment.STATIC;
                String ip = Global.getString(mContext.getContentResolver(), Global.ETHERNET_STATIC_IP);
				if(isIpAddress(ip)) {
                    mIpaddr.setIp(ip);
                }
                String mask = Global.getString(mContext.getContentResolver(), Global.ETHERNET_STATIC_MASK);
                if(isIpAddress(mask)) {
                    mMask.setIp(mask);
                }
                String gateway =  Global.getString(mContext.getContentResolver(), Global.ETHERNET_STATIC_GATEWAY);
				if(isIpAddress(gateway)) {
                    mGw.setIp(gateway);
                }
                String dns1 = Global.getString(mContext.getContentResolver(), Global.ETHERNET_STATIC_DNS1);
                if(isIpAddress(dns1)) {
                    mDns1.setIp(dns1);
                }
                String dns2 = Global.getString(mContext.getContentResolver(), Global.ETHERNET_STATIC_DNS2);
                if(isIpAddress(dns2)) {
                    mDns1.setIp(dns2);
                }
            }
        });

        mConTypeDhcp.setOnClickListener(new RadioButton.OnClickListener() {
            public void onClick(View v) {
                mIpaddr.setEnabled(false);
                mDns1.setEnabled(false);
                mDns2.setEnabled(false);
                mGw.setEnabled(false);
                mMask.setEnabled(false);
				//mprefix.setEnabled(false);
				mIpAssignment = IpAssignment.DHCP;
                mIpaddr.clear();
                mDns1.clear();
                mDns2.clear();
                mGw.clear();
                mMask.clear();
				//mprefix.setText("");
            }
        });

        this.setInverseBackgroundForced(true);
        this.setButton(BUTTON_POSITIVE, context.getText(R.string.menu_save), this);
        this.setButton(BUTTON_NEGATIVE, context.getText(R.string.menu_cancel), this);
		UpdateInfo();
		return 0;
    }

    private String formatIpAddresses(LinkProperties prop) {
        if (prop == null) return null;
        Iterator<InetAddress> iter = prop.getAllAddresses().iterator();
        // If there are no entries, return null
        if (!iter.hasNext()) return null;
        // Concatenate all available addresses, comma separated
        String addresses = "";
        while (iter.hasNext()) {
            addresses += iter.next().getHostAddress();
            if (iter.hasNext()) addresses += "\n";
        }
        return addresses;
    }

    private Inet4Address getIPv4Address(String text) {
        try {
            return (Inet4Address) NetworkUtils.numericToInetAddress(text);
        } catch (IllegalArgumentException|ClassCastException e) {
            return null;
        }
    }

    private void saveSettings() {
        Global.putString(mContext.getContentResolver(), Global.ETHERNET_STATIC_IP,
                    mIpaddr.getText());
        Global.putString(mContext.getContentResolver(), Global.ETHERNET_STATIC_MASK,
                    mMask.getText());
        Global.putString(mContext.getContentResolver(), Global.ETHERNET_STATIC_GATEWAY,
                    mGw.getText());
        Global.putString(mContext.getContentResolver(), Global.ETHERNET_STATIC_DNS1,
                    mDns1.getText());
        Global.putString(mContext.getContentResolver(), Global.ETHERNET_STATIC_DNS2,
                    mDns2.getText());
    }

    private void handle_saveconf() {
        if (mConTypeDhcp.isChecked()) {
			Slog.i(TAG,"mode dhcp");
			mEthManager.setConfiguration(new IpConfiguration(mIpAssignment, ProxySettings.NONE,
                                null, null));
        } else {
            Slog.i(TAG,"mode static ip");
            if(TextUtils.isEmpty(mIpaddr.getText())
                    || TextUtils.isEmpty(mMask.getText())
					|| TextUtils.isEmpty(mGw.getText())
                    || (TextUtils.isEmpty(mDns1.getText()) && TextUtils.isEmpty(mDns2.getText()))) {
                Toast.makeText(mContext, R.string.eth_settings_empty, Toast.LENGTH_SHORT).show();
            } else if (!isIpAddress(mIpaddr.getText())
                    || !isIpAddress(mMask.getText())
				    || !isIpAddress(mGw.getText())
				    || !(isIpAddress(mDns1.getText()) || isIpAddress(mDns2.getText()))) {
                Toast.makeText(mContext, R.string.eth_settings_not_complete, Toast.LENGTH_SHORT).show();
			} else if (isIpAddress(mIpaddr.getText())
                    && isIpAddress(mMask.getText())
				    && isIpAddress(mGw.getText())
				    && (isIpAddress(mDns1.getText()) || isIpAddress(mDns2.getText()))) {
				try {
				mStaticIpConfiguration = new StaticIpConfiguration();
                mIpConfiguration = new IpConfiguration();

                mStaticIpConfiguration.ipAddress = new LinkAddress(InetAddress.getByName(mIpaddr.getText()), 24);
                mStaticIpConfiguration.gateway = InetAddress.getByName(mGw.getText());
                mStaticIpConfiguration.domains = mMask.getText();
                if (isIpAddress(mDns1.getText())) {
                    mStaticIpConfiguration.dnsServers.add(InetAddress.getByName(mDns1.getText()));
                }
                if (isIpAddress(mDns2.getText())) {
                    mStaticIpConfiguration.dnsServers.add(InetAddress.getByName(mDns2.getText()));
                }
                mIpConfiguration.ipAssignment = IpAssignment.STATIC;
                mIpConfiguration.proxySettings = ProxySettings.STATIC;
                mIpConfiguration.staticIpConfiguration = mStaticIpConfiguration;	

                saveSettings();
                mEthManager.setConfiguration(mIpConfiguration);

                } catch (UnknownHostException e) {
                }

			} else {
				Toast.makeText(mContext, R.string.eth_settings_error, Toast.LENGTH_LONG).show();
            }
		}
    }

    private boolean isIpAddress(String value) {
        int start = 0;
        int end = value.indexOf('.');
        int numBlocks = 0;

        while (start < value.length()) {
            if (end == -1) {
                end = value.length();
            }

            try {
                int block = Integer.parseInt(value.substring(start, end));
                if ((block > 255) || (block < 0)) {
                        return false;
                }
            } catch (NumberFormatException e) {
                    return false;
            }

            numBlocks++;

            start = end + 1;
            end = value.indexOf('.', start);
        }
        return numBlocks == 4;
    }

    public void onClick(DialogInterface dialog, int which) {
        switch (which) {
            case BUTTON_POSITIVE:
                handle_saveconf();
                break;
            case BUTTON_NEGATIVE:
                //Don't need to do anything
                break;
            default:
        }
    }

}
